packer {
  required_plugins {
    googlecompute = {
      version = ">= 0.0.1"
      source = "github.com/hashicorp/googlecompute"
    }
  }
}

source "googlecompute" "ansiblevault" {
  project_id = "turnkey-triumph-337815"
  account_file = "image-builder-sa.json"
  source_image = "ubuntu-1804-bionic-v20211021"
  ssh_username = "packer"
  zone = "us-central1-a"
}

build {
  sources = [
      "sources.googlecompute.ansiblevault"
      ]
   provisioner "ansible" {
       playbook_file = "./vault.yml"
    }
   
   provisioner "shell" {
     inline = [
       "vault --version",
       "export VAULT_ADDR=http://0.0.0.0:8200",
       "vault operator init"
     ]
   }
}